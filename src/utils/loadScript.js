function loadScript(src, callback) {
  const cb = callback || (() => {})
  const $script = document.createElement('script')
  $script.src = src
  $script.id = src
  document.body.appendChild($script)
  const onEnd = 'onload' in $script ? stdOnEnd : ieOnEnd
  onEnd($script)

  function stdOnEnd(script) {
    const _this = this
    script.onload = () => {
      if (_this) {
        _this.onerror = _this.onload = null
      }
      cb(null, script)
    }
    script.onerror = () => {
      if (_this) {
        _this.onerror = _this.onload = null
      }
      cb(new Error(`Failed to load ${src}`), script)
    }
  }

  function ieOnEnd(script) {
    script.onreadystatechange = () => {
      if (this.readyState !== 'complete' && this.readyState !== 'loaded') return
      this.onreadystatechange = null
      cb(null, script)
    }
  }
}

export default loadScript

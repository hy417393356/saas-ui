import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/redisMng/loadData'
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/redisMng/toView',
    method: 'post',
    data
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/redisMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/redisMng/doDelete',
    method: 'post',
    data
  })
}

export function delAll(data) {
  return request({
    url: 'admin-service/api/redisMng/doDeleteAll',
    method: 'post',
    data
  })
}

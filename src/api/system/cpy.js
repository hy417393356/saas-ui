import request from '@/utils/request'

export function loadDataUrl() {
  return 'admin-service/api/cpyMng/loadData'
}

export function exportUrl() {
  return 'admin-service/api/cpyMng/doExport'
}

export function importTemplateUrl() {
  return 'admin-service/api/cpyMng/downloadImportTemplate'
}

export function importUrl() {
  return 'admin-service/api/cpyMng/doImport'
}

export function printUrl() {
  return 'admin-service/api/cpyMng/doPrint'
}

export function deleteUrl() {
  return 'admin-service/api/cpyMng/doDelete'
}

export function toAdd(data) {
  return request({
    url: 'admin-service/api/cpyMng/toAdd',
    method: 'post',
    data
  })
}

export function toCopy(data) {
  return request({
    url: 'admin-service/api/cpyMng/toCopy',
    method: 'post',
    data
  })
}

export function viewMainData(data) {
  return request({
    url: 'admin-service/api/cpyMng/toView',
    method: 'post',
    data
  })
}

export function getAllCpy(cpyName) {
  return request({
    url: 'admin-service/api/cpyMng/getAllCpy',
    method: 'get',
    params: {
      cpyName
    }
  })
}

export function addOrEdit(data) {
  return request({
    url: 'admin-service/api/cpyMng/doSave',
    method: 'post',
    data
  })
}

export function del(data) {
  return request({
    url: 'admin-service/api/cpyMng/doDelete',
    method: 'post',
    data
  })
}


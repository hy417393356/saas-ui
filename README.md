<h1 style="text-align: center">简云Saas平台</h1>


 **产品分三个版本:
  1. 开源版本(包含了系统基础架构+在线表单设计). 此版本代码完全开源
  2. ERP版本(在开源版本基础上增加了ERP业务模块, 在线报表开发, 工作流设计, 代码生成模块, 移动端进销存).
     有意向合作的个人或企业欢迎咨询)** 
  3. 开放个人学习版本(加入知识星球即可获取链接下载)

 
**简云商用版本体验地址**      [简云Saas平台]http://47.114.158.164:8080/yhy/#/dashboard)

**简云商用版本移动端H5体验地址**      [简云Saas平台](http://47.114.158.164:8080/yhy-h5/)

###  _商用版本金桔云ERP 1.0版本上线试运行，欢迎体验:_ 
  [金桔云ERP](https://eam.basicredit.com/orange-erp/)
  [操作文档说明链接](https://www.yuque.com/yanghuiyuan-qlsxr/orange-erp)

<div style="text-align: center">

[![Git Star] (https://gitee.com/hy417393356/saas-java)
</div>
加微信或QQ: 417393356

![QQ](yhy-web/src/sql/qq.jpg)


![知识星球](yhy-web/src/sql/%E7%9F%A5%E8%AF%86%E6%98%9F%E7%90%83.jpg)



#### 项目简介
简云Saas平台 基于SpringBoot2.2.0, Mybatis, JWT, Redis, VUE+Element-UI 的前后端分离的Saas平台后台管理系统





#### 项目源码

|     |   后端源码  |   前端源码  |
|---  |--- | --- |
|  码云   |  https://gitee.com/hy417393356/saas-java   |  https://gitee.com/hy417393356/saas-ui   |

####  系统功能

- 公司信息管理:      公司信息的维护及配置.
- 用户管理：         用户信息的维护及配置.
- 菜单管理：         菜单管理维护，动态配置菜单,按钮，字段权限.
- 我的组织机构管理:  可配置系统组织架构，树形表格展示
- 角色管理：        角色信息维护，可对数据权限(通过机构部门控制)，功能权限，用户与菜单进行分配。

- 字典管理：        配置一些常用固定的数据.
- 打印导出模板管理  用户可自定义配置报表输出格式(支持JXLS)

## - 自定义表单: 用于OA表单自定义申请, 自定义模块扩展功能.
## －表单业务申请: 表单业务申请，提交
## - 表格自定义列配置名称，宽度，列的顺序

 **增加批量打印单据功能** 

--工作流定义: 集成了activity表单设计，部署，导入功能
- 工作流配置: 按模块+表单设计 工作流。

- 系统缓存：  使用RedisTemplate操作， 将redis可视化提供基本的操作.
- SQL监控：   使用druid 监控数据库
- 定时任务：  整合Quartz做定时任务.
- 代码生成：  根据表名自动生成前后端CRUD代码，可节省 60%代码开发率.


#### 项目结构
项目采用多模块开发方式(见下图)
 admin: 系统管理
 base: 基础数据
 business: 业务模块
 report: 报表模块
 api: 与第三方系统对接
 workflow: 工作流模块
 form: 表单定义模块
 job: 定时任务

<img src="https://images.gitee.com/uploads/images/2019/0904/172432_1ad97d33_1714834.png"/>
#### 系统预览
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101153_b6ab4ec5_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101213_4312698f_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101226_360193ec_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2020/0428/101241_621dbd99_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_d38bf99b_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_0c06dccd_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_75229eed_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_9edd937e_1714834.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_8ec89aac_1714834.png"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0904/172432_e7cbff11_1714834.png"/></td>
    </tr>
    <tr>   
</table>

说明: 

启动时在Idea增加配置:
--spring.profiles.active=dev

![输入图片说明](https://images.gitee.com/uploads/images/2021/0608/221936_8a544b2c_1714834.png "屏幕截图.png")

数据库密码配置:
    ![输入图片说明](https://images.gitee.com/uploads/images/2021/0608/222000_9f66204e_1714834.png "屏幕截图.png")

aspose.cells jar包找不到, 请参考 yhy-common/lib下的 readme.txt 文件操作



#### 反馈交流
作者QQ: 417393356